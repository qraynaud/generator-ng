'use strict';

var util = require('util');
var ComponentBase = require('../libs/ComponentBase');

var DirectiveGenerator = module.exports = function DirectiveGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  ComponentBase.apply(this, arguments);
};

util.inherits(DirectiveGenerator, ComponentBase);
ComponentBase.defineType(DirectiveGenerator, 'directive');
