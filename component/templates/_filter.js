angular.module('<%= module %>')
.filter('<%= camelFullName %>', function() {
  'use strict';

  return function(input /*, arguments */) {
    // Right now, it is only an ID filter
    return input;
  };
});
