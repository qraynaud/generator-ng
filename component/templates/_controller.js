angular.module('<%= module %>')
.controller('<%= camelFullNameUCFirst %>', function($scope) {
  'use strict';

  $scope.ctrlName = '<%= camelFullNameUCFirst %>';
});
