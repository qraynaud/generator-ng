angular.module('<%= module %>')
// Use either service
.service('<%= camelFullNameUCFirst %>', function() {
  'use strict';

  return null;
})
/* Or factory
.factory('<%= camelFullNameUCFirst %>', function() {
  'use strict';

  return null;
})
*/
;
