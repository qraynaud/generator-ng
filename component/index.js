'use strict';

var util = require('util');
var yeoman = require('yeoman-generator');
var path = require('path');
var fs = require('fs');
var conf = require('../libs/conf.js');
var inflect = require('i')(true);

var ComponentGenerator = module.exports = function ComponentGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  yeoman.generators.Base.apply(this, arguments);

  this.argument('type', {
    desc: 'Type of component to create',
    required: true,
    type: 'String'
  });

  this.argument('module', {
    desc: 'Module name to create the component into',
    required: false,
    type: 'String'
  });

  this.argument('name', {
    desc: 'Name of the component to create',
    required: false,
    type: 'String'
  });

  switch (this.type) {
  case 'controller':
  case 'directive':
  case 'filter':
  case 'service':
    break;
  default:
    var msg = 'Unkown type "' + this.type + '" for components.';
    this.log
    .write()
    .error('Error: ' + msg);
    throw new Error(msg);
  }
};

util.inherits(ComponentGenerator, yeoman.generators.Base);

ComponentGenerator.prototype.askConf = function askConf() {
  if (this.module && this.name)
    return;

  var cb = this.async();

  var prompts = [];
  if (!this.module) {
    var moduleConf = {
      name: 'module',
      message: 'In what module should be generated the new ' + this.type + '?',
      default: conf.val('lastModule')
    };
    if (!moduleConf.default)
      delete moduleConf.default;
    prompts.push(moduleConf);
  }
  if (!this.name) {
    prompts.push({
      name: 'name',
      message: 'What name should we give to this new ' + this.type + '?'
    });
  }

  this.prompt(prompts, function(props) {
    if (!this.name) this.name = props.name;
    if (!this.module) this.module = props.module;

    if (conf.val('lastModule') !== this.module)
      conf.val('lastModule', this.module).save();

    cb();
  }.bind(this));
};

ComponentGenerator.prototype.setPrefix = function setPrefix() {
  this.prefix = conf.val(this.module + '.prefix');
  if (this.prefix)
    return;

  var cb = this.async();
  var prompts = [{
    name: 'prefix',
    message: 'What prefix should we use for the components in this module?',
    default: this.module
  }];

  this.prompt(prompts, function(props) {
    this.prefix = props.prefix;
    conf.val(this.module + '.prefix', this.prefix).save();

    cb();
  }.bind(this));
};

ComponentGenerator.prototype.createNames = function createNames() {
  this.camelPrefix = inflect.camelize(inflect.underscore(this.prefix));
  this.camelName = inflect.camelize(inflect.underscore(this.name));

  this.fullName = this.prefix + '-' + this.name;
  this.camelFullName = this.camelPrefix.charAt(0).toLowerCase() +
    this.camelPrefix.slice(1) + this.camelName;
  this.camelFullNameUCFirst = this.camelPrefix + this.camelName;
};

ComponentGenerator.prototype.files = function files() {
  var src = '_component.js';
  if (fs.existsSync(path.join(__dirname, 'templates', '_' + this.type + '.js')))
    src = '_' + this.type + '.js';

  this.template(
    src,
    path.join('client/js', this.module, this.type + 's', this.name + '.js')
  );
};
