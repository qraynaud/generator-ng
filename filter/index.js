'use strict';

var util = require('util');
var ComponentBase = require('../libs/ComponentBase');

var FilterGenerator = module.exports = function FilterGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  ComponentBase.apply(this, arguments);
};

util.inherits(FilterGenerator, ComponentBase);
ComponentBase.defineType(FilterGenerator, 'filter');
