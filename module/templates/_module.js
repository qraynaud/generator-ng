angular.module('<%= name %>', [])
.constant('<%= name %>Version', jQuery('html').data('app-version'))<% if (useRouteProvider) { %>
.config(function($routeProvider, <%= name %>Version) {
  'use strict';

  $routeProvider
  .when('/home', {
    templateUrl: '/' + <%= name %>Version + '/templates/home.html',
    controller: '<%= camelPrefix %>Home'
  })
  .otherwise({ redirectTo: '/home' });
})<% } %>;
