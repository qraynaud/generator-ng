'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var path = require('path');
var conf = require('../libs/conf.js');
var inflect = require('i')(true);

var ModuleGenerator = module.exports = function ModuleGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  yeoman.generators.Base.apply(this, arguments);

  this.argument('name', {
    desc: 'Name of the module to create',
    required: false,
    type: 'String'
  });

  this.hookFor('ng:controller', {
    args: [this.name, 'main'],
    options: {
      'skip-install': true
    }
  });

  var hookFor = this.hookFor;
  this.hookFor = function() {
    this._running = false;
    hookFor.apply(this, arguments);
    this._running = false;
  }.bind(this);
};

util.inherits(ModuleGenerator, yeoman.generators.Base);

ModuleGenerator.prototype.askConf = function askConf() {
  if (this.name)
    return;

  var cb = this.async();

  var prompts = [{
    name: 'name',
    message: 'What name should we give to this new module?'
  }];

  this.prompt(prompts, function (props) {
    // Copy configuration to this object
    this.name = props.name;

    // Shitty workaround to define module's name in the subgenerator
    this._hooks[0].args[0] = this.name;

    cb();
  }.bind(this));
};

ModuleGenerator.prototype.saveLastModule = function saveLastModule() {
  conf.val('lastModule', this.name).save();
};

ModuleGenerator.prototype.askPrefix = function askPrefix() {
  var cb = this.async();

  var prompts = [{
    name: 'prefix',
    message: 'What prefix should we use to generate object for the "' +
      this.name + '" module?',
    default: this.name
  }];

  this.prompt(prompts, function (props) {
    // Copy configuration to this object
    this.prefix = props.prefix;
    this.camelPrefix = inflect.camelize(inflect.underscore(this.prefix));

    // Add the value to the configuration
    conf.val(this.name + '.prefix', this.prefix).save();

    cb();
  }.bind(this));
};

ModuleGenerator.prototype.askRouting = function askRouting() {
  var cb = this.async();

  var prompts = [{
    type: 'confirm',
    name: 'useRouteProvider',
    message: 'Do you want to use $routeProvider in the "' +
      this.name + '" module?',
    default: false
  }];

  this.prompt(prompts, function (props) {
    // Copy configuration to this object
    this.useRouteProvider = props.useRouteProvider;

    // Add the value to the configuration
    conf.val(this.useRouteProvider + '.prefix', this.useRouteProvider).save();

    if (this.useRouteProvider) {
      this.hookFor('ng:controller', {
        args: [this.name, 'home'],
        options: {
          'skip-install': true
        }
      });
    }

    cb();
  }.bind(this));
};

ModuleGenerator.prototype.files = function files() {
  this.mkdir(path.join('client/js', this.name));
  this.mkdir(path.join('client/js', this.name, 'controllers'));
  this.mkdir(path.join('client/js', this.name, 'directives'));
  this.mkdir(path.join('client/js', this.name, 'filters'));
  this.mkdir(path.join('client/js', this.name, 'services'));

  this.copy('_module.js', path.join('client/js', this.name, this.name + '.js'));
};
