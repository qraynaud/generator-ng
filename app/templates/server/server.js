var express = require('express');
var app = express();
var pkg = require('../package.json');

app.use(express.static('public/'));

app.get('/', express.static('public/index.html'));

app.listen(pkg.app.port);
console.log('Listening on port ' + pkg.app.port + ' - ' + app.settings.env + ' mode');
