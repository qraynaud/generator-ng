'use strict';

module.exports = function(grunt) {
  var pkg = grunt.file.readJSON('package.json');

  // Project configuration
  var config = {
    pkg: pkg,

    clean: {
      all: ['public/'],

      release: [
        'public/<%= pkg.version %>/css/dev'
      ]
    },

    jade: {
      options: {
        pretty: true,
        compileDebug: false,
        data: {
          pkg: pkg
        }
      },

      compile: {
        files: [{
          expand: true,
          cwd: 'client/html',
          src: '*.jade',
          dest: 'public',
          ext: '.html'
        }, {
          expand: true,
          cwd: 'client/html/templates',
          src: '*.jade',
          dest: 'public/<%= pkg.version %>/templates',
          ext: '.html'
        }]
      }
    },

    stylus: {
      compile: {
        files: [{
          src: 'client/css/main.styl',
          dest: 'public/<%= pkg.version %>/css/dev/app.css'
        }]
      }
    },

    watch: {
      jade: {
        files: 'client/html/**/*.jade',
        tasks: ['jade', 'usemin2']
      },
      stylus: {
        files: 'client/css/**/*.styl',
        tasks: ['stylus', 'jade', 'usemin2']
      },
      js: {
        files: 'client/js/**/*.js',
        tasks: ['jade', 'usemin2']
      }
    },

    usemin2: {
      html: 'public/*.html',
      options: {
        baseDir: 'public',
        jsmin: ['ngmin', 'uglify']
      },
      css: {
        app: {
          dest: 'public/<%= pkg.version %>/css/app.min.css',
          files: [{
            cwd: 'public/<%= pkg.version %>/css/dev/',
            src: '*.css'
          }, {
            cwd: 'bower_components/bootstrap/dist/css/',
            src: 'bootstrap__min__.css',
            dest: 'public/<%= pkg.version %>/css/libs/'
          }]
        }
      },
      js: {
        head: {
          dest: 'public/<%= pkg.version %>/js/libs.min.js',
          files: [{
            cwd: 'bower_components/jquery/',
            src: 'jquery__min__.js',
            dest: 'public/<%= pkg.version %>/js/libs/'
          }, {
            cwd: 'bower_components/bootstrap/dist/js/',
            src: 'bootstrap__min__.js',
            dest: 'public/<%= pkg.version %>/js/libs/'
          }, {
            cwd: 'bower_components/angular/',
            src: 'angular__min__.js',
            dest: 'public/<%= pkg.version %>/js/libs/'
          }]
        },
        body: {
          dest: 'public/<%= pkg.version %>/js/body.min.js',
          files: [{
            cwd: 'client/js',
            src: ['*.js', '*/*.js', '*/*/**.js'],
            dest: 'public/<%= pkg.version %>/js/'
          }]
        }
      }
    }
  };

  grunt.initConfig(config);

  // Load modules
  grunt.file.expand('node_modules/grunt-*/tasks').forEach(grunt.loadTasks);

  // Common tasks
  grunt.registerTask('common-build', 'Common steps in all builds.', [
    'clean:all',
    'jade',
    'stylus'
  ]);

  // Dev build
  grunt.registerTask('dev', 'Configure project for development.', [
    'common-build',
    'usemin2:dev'
  ]);

  // Release build
  grunt.registerTask('release', 'Configure project for release.', [
    'common-build',
    'usemin2:release',
    'clean:release'
  ]);

  // Default task
  grunt.registerTask('default', ['release']);
};
