'use strict';

var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');

var NgGenerator = module.exports = function NgGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);

  this.on('end', function () {
    this.installDependencies({ skipInstall: options['skip-install'] });
  });

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));

  this.hookFor('ng:module', {
    args: ['application', true],
    options: {
      'skip-install': true
    }
  });
};

util.inherits(NgGenerator, yeoman.generators.Base);

NgGenerator.prototype.genericAppConfig = function genericAppConfig() {
  var cb = this.async();

  // have Yeoman greet the user.
  console.log(this.yeoman);

  var prompts = [{
    name: 'appName',
    message: 'What name would you like to give to your application?',
    default: path.basename(path.resolve(process.cwd()))
  }, {
    name: 'appVersion',
    message: 'What version would you like to give to your application?',
    default: '0.0.1'
  }];

  this.prompt(prompts, function (props) {
    // Copy configuration to this object
    this.appName = props.appName;
    this.appVersion = props.appVersion;

    // Shitty workaround to define appName in the subgenerator
    this._hooks[0].args[0] = this.appName;

    cb();
  }.bind(this));
};

NgGenerator.prototype.bootstrapConfig = function bootstrapConfig() {
  var cb = this.async();

  var prompts = [{
    type: 'confirm',
    name: 'useBootstrap',
    message: 'Do you want to use bootstrap libraries in your application?',
    default: true
  }];

  this.prompt(prompts, function (props) {
    this.useBootstrap = props.useBootstrap;

    if (!this.useBootstrap)
      return cb();

    this.prompt({
      name: 'bootstrapVersion',
      message: 'What version/branch of bootstrap would you like to use?',
      default: '~3.0.0-rc1'
    }, function (props) {
      this.bootstrapVersion = props.bootstrapVersion;
      cb();
    }.bind(this));

  }.bind(this));
};

NgGenerator.prototype.app = function app() {
  this.copy('_package.json', 'package.json');
  this.copy('_bower.json', 'bower.json');

  this.mkdir('client');
  this.mkdir('client/css');
  this.mkdir('client/html');
  this.mkdir('client/html/layouts');
  this.mkdir('client/html/templates');
  this.mkdir('client/js');

  this.mkdir('server');
  this.mkdir('server/libs');
  this.mkdir('server/routes');

  this._engine = null;
  this.copy('Gruntfile.js', 'Gruntfile.js');

  this.copy('server/server.js', 'server/server.js');

  this.copy('client/css/main.styl', 'client/css/main.styl');
  this.copy('client/html/index.jade', 'client/html/index.jade');
  this.copy('client/html/layouts/basic.jade', 'client/html/layouts/basic.jade');
  this.copy('client/html/templates/home.jade', 'client/html/templates/home.jade');
};

NgGenerator.prototype.useEditorConfig = function useEditorConfig() {
  var cb = this.async();

  var prompts = [{
    type: 'confirm',
    name: 'useEditorConfig',
    message: 'Do you want to use EditorConfig for your application?',
    default: true
  }];

  this.prompt(prompts, function (props) {
    if (props.useEditorConfig)
      this.copy('editorconfig', '.editorconfig');

    cb();
  }.bind(this));
};
