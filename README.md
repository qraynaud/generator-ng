# Generator-ng
WiP - Will be an industrialized repository to create application using Angular JS

## Getting started
- Make sure you have [yo](https://github.com/yeoman/yo) installed:
    `npm install -g yo`
- Install the generator: `npm install -g generator-ng`
- Run: `yo ng`

## License
[MIT License](http://en.wikipedia.org/wiki/MIT_License)
