'use strict';

var fs = require('fs');
var path = require('path');

function addToObj(obj, path, value) {
  if (path.length === 1) {
    obj[path[0]] = value;
    return obj;
  }

  var prop = path.shift();
  obj[prop] = addToObj(obj[prop] || {}, path, value);
  return obj;
}

function getFromObj(obj, path) {
  if (!path.length)
    return obj;

  var attr = path.shift();
  if (!(attr in obj))
    return undefined;

  return getFromObj(obj[attr], path);
}

function Conf() {
  this.load();
}
Conf.prototype = {
  _file: path.join(process.cwd(), '.ngapp.json'),
  _values: {},

  load: function() {
    if (!fs.existsSync(this._file))
      return;
    this._values = JSON.parse(fs.readFileSync(this._file));
    return this;
  },
  val: function(key, value) {
    if (value === undefined)
      return getFromObj(this._values, key.split('.'));

    addToObj(this._values, key.split('.'), value);
    return this;
  },
  save: function() {
    fs.writeFileSync(this._file, JSON.stringify(this._values));

    return this;
  }
};

module.exports = new Conf();
