'use strict';

var util = require('util');
var yeoman = require('yeoman-generator');

var ComponentBase = module.exports = function ComponentBase(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  yeoman.generators.Base.apply(this, arguments);

  this.argument('module', {
    desc: 'Module name to create the component into',
    required: false,
    type: 'String'
  });

  this.argument('name', {
    desc: 'Name of the component to create',
    required: false,
    type: 'String'
  });

  this.hookFor('ng:component', {
    args: [this.componentType, this.module, this.name],
    options: {
      'skip-install': true
    }
  });
};
ComponentBase.defineType = function(ctor, type) {
  Object.defineProperty(ctor.prototype, 'componentType', {
    value: type
  });
};

util.inherits(ComponentBase, yeoman.generators.Base);
