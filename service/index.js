'use strict';
var util = require('util');
var ComponentBase = require('../libs/ComponentBase');

var ServiceGenerator = module.exports = function ServiceGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  ComponentBase.apply(this, arguments);
};

util.inherits(ServiceGenerator, ComponentBase);
ComponentBase.defineType(ServiceGenerator, 'service');
